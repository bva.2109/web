<?php

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('sword');
});

Route::get('/ohs', function () {
    return view('one handed sword');
});

Route::get('/ths', function () {
    return view('two-handed sword');
});
Route::get('/feedback', function () {
    return view('feedback');
});


Route::get('/db_view', function () {
    return view('db_view');
});

Route::get('contact-form', 'testerController@create');
Route::post('contact-form', 'App\Http\Controllers\testerController@store');

Route::name('user.')->group(function(){
    Route::view('/private','/loginout/private')->middleware('auth')->name('private');

    Route::get('/login',function(){
        if(Auth::check())
        {
            return redirect(route('user.private'));
        }
        return view('/loginout/login');
    })->name('login');

    Route::post('/login',[\App\Http\Controllers\LoginController::class,'login']);
   
    Route::get('/logout',function(){
        Auth::logout();
        setcookie('name',$_COOKIE['name'],time()-200);
        return redirect(route('user.login'));
    })->name('logout');
    
    Route::get('/registration',function(){
        if(Auth::check())
        {
            return redirect(route('user.private'));
        }
        return view('/loginout/registration');
    })->name('registration');

    Route::post('/registration',[\App\Http\Controllers\RegistrationController::class,'save']);

    Route::get('/login/{locale}', function ($locale) {
        if (! in_array($locale, ['en', 'ru'])) {
            abort(400);
        }
    
        App::setLocale($locale);
        return view('/loginout/login');
        //
    });

    Route::get('/registration/{locale}', function ($locale) {
        if (! in_array($locale, ['en', 'ru'])) {
            abort(400);
        }
    
        App::setLocale($locale);
        return view('/loginout/registration');
        //
    });

});