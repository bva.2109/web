<html>
<head>
<link href="../css/form.css" rel="stylesheet" />

</head>

<body>
<script type="text/javascript">
	function validateName(phone){
 		var regex = /^[A-Z,А-Я][a-z,а-я]*$/;
 		return regex.test(phone);
	}
	function valid() {
		
		var name=document.getElementById("name");
		var email=document.getElementById("email");
		var password=document.getElementById("password");
		var weight=document.getElementById("weight");
		var phone=document.getElementById("tphone");

		if (!name.value) {
			alert('Пожалуйста, введите Ваше имя');
			name.style.border="2px solid red";
			return false;
		}
		if (!validateName(name.value)) {
			alert('Пожалуйста, введите Ваше имя');
			name.style.border="2px solid red";
			return false;
		}
		
		if (!email.value) {
		alert('Пожалуйста, введите ваш email');
			email.style.border="2px solid red";
			return false;
		}

		if(!password.value)
		{
			alert('Пожалуйста, введите ваш пароль');
			password.style.border="2px solid red";
			return false;
		}
		if(password.value.length<3)
		{
			alert('Пароль должен быть не меннее 3 занков');
			password.style.border="2px solid red";
			return false;
		}

		if(weight.value>300)
		{
			alert('Вес должен быть не более 300');
			weight.style.border="2px solid red";
			return false;
		}
		if(!phone.value)
		{
			alert('Пожалуйста, введите номер телефона');
			phone.style.border="2px solid red";
			return false
		}
		if(!validatePhone(phone))
		{
			alert('Пожалуйста, введите номер телефона');
			phone.style.border="2px solid red";
			return false;
		}
		return true;

	};

	
</script>

<form action="swrod.html" onsubmit="return valid();">
   <div id="inp">
        <label>Name:<font color="red">*</font></label>
        <input  type="text" id="name"/>
	</div>
	<div id="inp">
        <label>E-mail:<font color="red">*</font></label>
        <input type="email" id="email"/>
	</div>
	<div id="inp">
		<label>Password:<font color="red">*</font></label>
        <input type="text" id="password"/>
	</div>
	<div id="inp">

		<label>weight:</label>
        <input type="number" id="weight"/>
	</div>
	<div id="inp">
		<label>Phone:<font color="red">*</font></label>
		<input type="text" id="tphone" />
	</div>
	<div id="inp">
        <button type="submit">Submit</button>
	</div>

</form>


</body>
</html>