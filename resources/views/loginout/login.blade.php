<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
<link href="../css/form.css" rel="stylesheet" />
<meta name ="csrf-token" content="{{csrf_token()}}">
</head>

<body>
<script type="text/javascript">
	
	function valid() {
		
		
		var email=document.getElementById("email");
		var password=document.getElementById("password");
		

		
		
		if (!email.value) {
		alert('Пожалуйста, введите ваш email');
			email.style.border="2px solid red";
			return false;
		}

		if(!password.value)
		{
			alert('Пожалуйста, введите ваш пароль');
			password.style.border="2px solid red";
			return false;
		}
		if(password.value.length<3)
		{
			alert('Пароль должен быть не меннее 3 занков');
			password.style.border="2px solid red";
			return false;
		}

		
		return true;

	};

	
</script>
<h1>{{__('login_lang.login')}}</h1>
<form method="POST" action="{{route('user.login')}}" >
	@csrf
	
	<div id="inp">
        <label>{{__('login_lang.email')}}<font color="red">*</font></label>
        <input name="email" type="email" id="email"/>
	</div>
	@error('email')
	<div class="alert alert-danger">{{$message}}</div>
	@enderror
	<div id="inp">
		<label>{{__('login_lang.password')}}<font color="red">*</font></label>
        <input name="password" type="text" id="password"/>
	</div>
	@error('password')
	<div class="alert alert-danger">{{$message}}</div>
	@enderror
	
	<div id="inp">
        <button type="submit">Submit</button>
	</div>

</form>


</body>
</html>