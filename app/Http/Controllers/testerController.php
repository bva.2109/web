<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\tester;

class testerController extends Controller
{
    public function create() { 

        return view('db_view'); 
       }  
  
       public function store(Request $request) { 
        $contact = new tester;
  
        $contact->name = $request->name;
        $contact->email = $request->email;
        $contact->save();
        
        return response()->json(['success'=>'Form is successfully submitted!']);
  
      }
}
