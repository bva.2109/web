<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class RegistrationController extends Controller
{
    public function save(Request $request)
    {

        if(Auth::check())
        {
            return redirect(route('user.private'));
        }

        $validation = $request->validate(
            [
                'name'=>'required',
                'email'=>'required|email',
                'password'=>'required',
            ]);
        $user = User::create($validation);
        if($user)
        {
            Auth::login($user);
            setcookie('name',$validation['name'],time()+300);
            return redirect(route('user.private'));
        }
        return redirect(route('user.login'))->withErrors([
            'formError'=>'Problem with save user'
        ]);
    }
    //
}
