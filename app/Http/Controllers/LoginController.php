<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $fromFields = $request->only(['email','password']);
        if(Auth::attempt($fromFields))
        {
            $name=User::where('email',$fromFields['email'])->value('name');
            setcookie('name',$name,time()+300);
            return redirect()->intended(route('user.private'));
        }
        return redirect(route('user.login'))->withErrors([
            'email'=>'Can not autorisate, try again']);
    }
    //
}
